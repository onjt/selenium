package com.onjt.selenium;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;



public class SeleniumApplication {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver-win64\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();

		driver.get("https://www.kibo.mg/");

		driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/input[2]")).sendKeys("biscuit");

		driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/button")).click();

		WebElement products = driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]"));

		WebElement firstArticle = products.findElement(By.cssSelector(":first-child"));

		firstArticle.click();

		driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[1]/div/span[3]/button[1]")).click();

		driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[2]/button")).click();

		driver.findElement(By.xpath("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[2]/div/div/a")).click();


	}

}
